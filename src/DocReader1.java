/*
 This class is the one that reads through the document, find and record tags for 
 each segment so the main class can deal them one at a time before finally 
 fininshing them up and recombining them into a final finished product document.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

class DocReader1 {

    File inputfile;
    String Title;
    PrintWriter createPerma;
    String fileName;

    /*
    This is the constructor.
     */
    public DocReader1(String title) {
        inputfile = new File(title);
        Title = title;
        
    }
    
    /*unneeded as of yet*/
    public boolean checkAccess(){
        return true;
    }
    /*This creates the Document to write the encrypted/decrypted version of the Document to.*/
    public void  CreateNewDoc(String title) {
        
        try {
            createPerma = new PrintWriter(title, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DocReader1.class.getName()).log(Level.SEVERE, null, ex);
        }
            fileName = title;
       
    }
    
    /*This is the main part of the DocReader1 that parses the input document, 
    and then calls the encryption/decryption oracle, taking it's output and 
    writing it to the created Document*/
    public void seeker(PeopleManager PM1) {
        try {
            /* The order of keys in the newer format is DL1 TS1 TS2 SC1 SC2 CF1 CF2*/
            Scanner spyglass;
            String Segment;
            String tags1;
            String[] tags2;
            Segment = "";
            spyglass = new Scanner(inputfile);
            EncryptionScheme encrypt = new EncryptionScheme(PM1);
            while (spyglass.hasNext()) {
                String awesome = spyglass.next();
//            Here is the case for encrypting unencrypted documents.
                if (awesome.equals("tags")) {
                    /*Here would be a good place to reformat the the document handling from "Title"--->"Encrypted_Document"------->"Decrypted_Document"
    to "Title"---->"Title_Encrypted"--->"Title_Decrypted".*/
                    if (Title.contains("_Decrypted")) {
                        Title = Title.substring(0, Title.length() - 10);
                    }
                     CreateNewDoc(Title + "_Encrypted");
                    createPerma.write("tagse" + "\n");
                    while (spyglass.hasNext() && !awesome.equals("ENDOFFILE")) {
                        tags1 = spyglass.nextLine();
                        tags2 = tags1.split(" ");
                        awesome = spyglass.nextLine();
                        while (!awesome.equals("ENDSEGMENT")) {
                            Segment += awesome + "\n";
                            awesome = spyglass.nextLine();
                        }
                        createPerma.write("tags" + tags1 + "\n");
                        createPerma.write(encrypt.encryptSegment(Segment, tags2));
                        createPerma.write("\nENDSEGMENT \n");
                        Segment = "";
                        awesome = spyglass.next();
                    }
                    createPerma.write("ENDOFFILE");
                    createPerma.close();
                }
                /*
Here is the case for decrypting already encrypted documents
                 */
                if (awesome.equals("tagse")) {
                    awesome = spyglass.nextLine();
                    /*Here would be a good place to reformat the the document handling from "Title"--->"Encrypted_Document"------->"Decrypted_Document"
    to "Title"---->"Title_Encrypted"--->"Title_Decrypted".*/
                    if (Title.contains("_Encrypted")) {
                        Title = Title.substring(0, Title.length() - 10);
                    }
                     CreateNewDoc(Title + "_Decrypted");
                    createPerma.write("tags" + "\n");
                    while (spyglass.hasNext() && !awesome.equals("ENDOFFILE")) {
                        tags1 = spyglass.nextLine();
                        int i = 0;
                        tags2 = tags1.split(" ");
                        String correctTag = null;
                        for (int h = 0; h < tags2.length; h++) {
                            for (int g = 0; g < PM1.hasAccess().length; g++) {
                                if (tags2[h].equals(PM1.hasAccess()[g])) {
                                    correctTag = PM1.hasAccess()[g];
                                    g = 1000000000;
                                    h = 1000000000;
                                }
                            }
                        }
                        awesome = spyglass.nextLine();
                        if (awesome.contains("\n")) {
                            awesome.replace("\n", "");
                        }
                        while (!awesome.equals("ENDSEGMENT")) {
                            Segment += awesome;
                            awesome = spyglass.next();
                        }

                        createPerma.write(tags1 + "\n");
                        createPerma.write(encrypt.decryptSegment(Segment, correctTag));
                        createPerma.write("\nENDSEGMENT \n");
                        Segment = "";
                        awesome = spyglass.next();
                    }
                    createPerma.write("ENDOFFILE");
                    createPerma.close();
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DocReader1.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
