
import java.util.Base64;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class EncryptionScheme {

    String[] allTags;

    PeopleManager PM2;

    /*This is the constructor*/
    public EncryptionScheme(PeopleManager PM1) {
        PM2=PM1;
        allTags = PM1.findAllTags();
    }

    /*This encrypts the key for the Encrypt Segment Method*/
    private static byte[] encryptKey(SecretKey b1, SecretKey s1)  {
        byte[] key2=null;
        try {
            byte[] key1;
            Cipher eK = Cipher.getInstance("AES/ECB/NoPadding");
            eK.init(Cipher.ENCRYPT_MODE, s1);
            key1 = eK.doFinal(b1.getEncoded());
            key2= key1;
            
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        } catch (BadPaddingException ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("The key seems to be corrupted, please exit the "
                    + "software and before restarting to try again.");
            System.exit(1);
        }
        return key2;
    }
    
    /*This decrypts the key for the Decrypt Segment Method*/
    private static byte[] decryptKey(byte[] b1, SecretKey s1) {
        try {
            Cipher eK = Cipher.getInstance("AES/ECB/NoPadding");
            eK.init(Cipher.DECRYPT_MODE, s1);
            b1 = eK.doFinal(b1);
            
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException  ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        } catch (BadPaddingException ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("The file seems to be corrupted, please select another "
                    + "file to try again.");
            System.exit(1);
        }
        return b1;
    }
    
    /*This is the method  that takes a segment, segment tags, and decrypts it.*/
    public String decryptSegment(String s1, String correctTag)  {
        String s2=null;
        try {
            Cipher c;
            c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            if (correctTag == null) {
                return s1;
            }
            SecretKey secretKey1;
            int keyPosition = 5;
            for(int i=0; i<allTags.length; i++){
                if(allTags[i].equals(correctTag)){
                    keyPosition=i;
                }
            }
            byte[] keys = new byte[allTags.length*16];
            byte[] keys1 = new byte[16];
            byte[] encrypted;
            byte[] decrypted;
            byte[] userIV;
            byte[] encryptedLine1;
            userIV = new byte[c.getBlockSize()];
            encrypted = Base64.getDecoder().decode(s1);
            encryptedLine1 = new byte[encrypted.length - (userIV.length + keys.length)];
            System.arraycopy(encrypted, 0, keys, 0, keys.length);
            System.arraycopy(encrypted, keys.length, userIV, 0, userIV.length);
            System.arraycopy(encrypted, (keys.length + userIV.length), encryptedLine1, 0, encryptedLine1.length);
            System.arraycopy(keys, keyPosition*16, keys1, 0, keys1.length);
            secretKey1 = new SecretKeySpec((decryptKey(keys1, PM2.accessMap().get(correctTag))), "AES");
            c.init(Cipher.DECRYPT_MODE, secretKey1, new IvParameterSpec(userIV));
            decrypted = c.doFinal(encryptedLine1);
            s2= new String(decrypted);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException  ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        } catch (BadPaddingException ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("The file seems to be corrupted, please select another "
                    + "file to try again.");
            System.exit(1);
        }
        return s2;
    }
    
    /*This is the method  that takes a segment, segment tags, user tags, and 
    encrypts it.*/
    public String encryptSegment(String s2, String[] segmentTags)  {
        String s3=null;
        try {
            /* Here is the random Key Generator.*/
            KeyGenerator keyGen;
            keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128);
            SecretKey secretKey = keyGen.generateKey();
            byte[] nullKey=new byte[16];
            byte[] b2 = new byte[allTags.length * 16];
            for (int i = 0; i < allTags.length; i++) {
                boolean tagUsed=false;
                for(int j=0; j<segmentTags.length; j++){
                    if(allTags[i].equals(segmentTags[j])){
                        tagUsed=true;
                        j=1000000000;
                    }
                }
                if (tagUsed==true && PM2.accessMap().get(allTags[i]) != null) {
                    System.arraycopy(encryptKey(secretKey, PM2.accessMap().get(allTags[i])), 0, b2, (i * 16), 16);
                } else{
                    System.arraycopy(nullKey, 0, b2, (i * 16), 16);
                }
                
            }
            Cipher c;
            c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] encrypted;
            byte[] userIV;
            byte[] encryptedLine;
            c.init(Cipher.ENCRYPT_MODE, secretKey);
            userIV = c.getIV();
            encrypted = c.doFinal((s2).getBytes());
            encryptedLine = new byte[(userIV.length) + (encrypted.length) + (b2.length)];
            System.arraycopy(b2, 0, encryptedLine, 0, b2.length);
            System.arraycopy(userIV, 0, encryptedLine, b2.length, userIV.length);
            System.arraycopy(encrypted, 0, encryptedLine, b2.length + userIV.length, encrypted.length);
            s3= Base64.getEncoder().encodeToString(encryptedLine);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        } catch (BadPaddingException ex) {
            Logger.getLogger(EncryptionScheme.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("This may not be a correctly formatted file or it"
                    + " has been corrupted. Please try a different file or an earlier version of the file");    
        }
        return s3;
    }

}
