import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/*
 This is the class that when accessed will look through either a database or some
 sort of a treemap-kind of resivoir for a database of people's names, their 
 passwords, and the tags that are associated with them. The different methods 
 will find the person in the database, make sure their password is the correct 
 one, and then either deny them access if they do not exist in the "database" or 
 if they are in there, look through their tags to find what encryptions they can 
 have decrypted for them. Also if the user's tags have a specific set of 
 characteristics (like being an administrator) they can add or remove users and 
 change or add tags to their file. a
 */
class PeopleManager {

    /*
    Pass around this object and then use methods (new methods) to hold the keys
    and then use PM1.returnkey (or another named function) as the key.
     */
    private String passWord;
    private String[] tags;
    private String tags1;
    private Map<String, SecretKey> m;

    /*This is the constructor*/
    public PeopleManager() {
        tags1 = "";
        tags = new String[7];
        m = this.makeMap(this.findAllTags(), this.findKey());

    }
    
    /*This is the method that sends the user's tags to the calling section code.*/
    public String[] hasAccess() {
        return tags;
    }
    
    /*This handles the verification of the username password combination input.*/
    public boolean findUser(String usrName1, String password1) {
        boolean usrExists = false;
        try {
            Statement statement;
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/Users_for_Spartan_Shield_Project", "leonidas", "thisissparta");
            String sql = "SELECT PASSWORD \nFROM User_Info \nWHERE USERNAME =\'" + usrName1 + "\'";
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {
                while (rs.next()) {
                    passWord = rs.getString("password");
                   // tags1 = rs.getString("tags");  
                }
            }
            tags = tags1.split(" ");
            usrExists = passWord.equals(password1);
            findUserTags(usrName1);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PeopleManager.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
        return usrExists;
    }
    
    /*This populates the String array of the user's tags*/
    public void findUserTags(String usrName1) {
        try {
            Statement statement;
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/Users_for_Spartan_Shield_Project", "leonidas", "thisissparta");
            String sql = "SELECT TAG \nFROM User_Tags \nWHERE USERNAME =\'" + usrName1 + "\'";
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {
                while (rs.next()) {
                    tags1 = rs.getString("tag");
                }
            }
            tags = tags1.split(" ");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PeopleManager.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
    }

    /*Added for future implementation*/
    public static void addPerson() {

    }
    
    /*Added for future implementation*/
    public static void removePerson() {

    }

    /*This populates the array of keys to make the map of information of tags to
    keys.*/
    public String[] findKey() {
        String[] s2 = null;
        try {
            Statement statement;
            String[] s1 = new String[7];
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/Users_for_Spartan_Shield_Project", "leonidas", "thisissparta");
            for (int i = 0; i < tags.length; i++) {
                String sql = "SELECT KEYSPEC \n FROM KEY_TABLE";
                statement = con.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                if (rs != null) {
                    int counter = 0;
                    while (rs.next()) {
                        s1[counter] = rs.getString("KEYSPEC");
                        counter++;
                    }
                }
            }

            s2 = s1;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PeopleManager.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
        return s2;
    }

    /*This populates the array of tags to make the map of information of tags to
    keys.*/
    public String[] findAllTags() {
        String[] s2 = null;
        try {
            Statement statement;
            String[] s1 = new String[7];
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/Users_for_Spartan_Shield_Project", "leonidas", "thisissparta");
            for (int i = 0; i < tags.length; i++) {
                String sql = "SELECT TAG \n FROM KEY_TABLE";
                statement = con.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                if (rs != null) {
                    int counter = 0;
                    while (rs.next()) {
                        s1[counter] = rs.getString("TAG");
                        counter++;
                    }
                }
            }
            s2 = s1;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PeopleManager.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
        return s2;
    }

    /*This allows the accessing of the map of information of tags to keys.*/
    public Map<String, SecretKey> accessMap() {
        return m;
    }

    /*This populates the map of information of tags to keys.*/
    public Map<String, SecretKey> makeMap(String[] s1, String[] ss1) {
        m = new HashMap<>();
        SecretKey[] sk1 = new SecretKey[ss1.length];
        for (int j = 0; j < ss1.length; j++) {
            sk1[j] = new SecretKeySpec(ss1[j].getBytes(), "AES");
        }
        for (int i = 0; i < s1.length; i++) {
            m.put(s1[i], sk1[i]);
        }
        return m;
    }

    /*This finds the correct key from the section tag given. Part of Future 
    Implementations.*/
    public SecretKey correctKey(String sectionTag) {
        SecretKey s1 = null;
        if (m.containsKey(sectionTag)) {
            s1 = m.get(sectionTag);
        }
        return s1;
    }
}