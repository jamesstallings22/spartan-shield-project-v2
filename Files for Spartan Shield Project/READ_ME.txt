Please see the User manual before using. The file named "PracticeFile" in this 
folder is correctly formatted all you have to do is change the tags and input the segments of text as you see fit. 
Also for Version 1.0 (the current version), the database file is included in the ZIP file you recieved, but you have
to physically connect the database in the NetBeans IDE for Java for the project to work
Thank you for using the Spartan Shield Project to protect your valuable data.
----James Stallings, Author, Designer, and Developer of the Project.